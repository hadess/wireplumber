/* WirePlumber
 *
 * Copyright © 2019 Collabora Ltd.
 *    @author George Kiagiadakis <george.kiagiadakis@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include "core.h"
#include "endpoint.h"
#include "error.h"
#include "factory.h"
#include "module.h"
#include "monitor.h"
#include "policy.h"
#include "properties.h"
#include "proxy.h"
#include "proxy-client.h"
#include "proxy-link.h"
#include "proxy-node.h"
#include "proxy-port.h"
#include "wpenums.h"
